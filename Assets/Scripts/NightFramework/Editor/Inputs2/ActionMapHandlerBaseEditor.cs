﻿using UnityEngine;
using UnityEditor;
using NightFramework.Inputs;

// ========================
// Revision 2021.01.30
// ========================

[CustomEditor(typeof(ActionMapHandlerBase), true)]
public class ActionMapHandlerBaseEditor : CustomEditorBase<ActionMapHandlerBase>
{
    // ========================================================================================
    private SerializedProperty _activateOnStartProperty;
    private SerializedProperty _deactivatePreviousOnActivateProperty;
    private SerializedProperty _activatePreviousOnDeactivateProperty;
    private SerializedProperty _useEventsProperty;
    private SerializedProperty _actionStartedEventsProperty;
    private SerializedProperty _actionPerformedEventsProperty;
    private SerializedProperty _actionCanceledEventsPainter;


    // ========================================================================================
    protected override void OnEnable()
    {
        base.OnEnable();

        _activateOnStartProperty = serializedObject.FindProperty(nameof(ActionMapHandlerBase.ActivateOnStart));
        _deactivatePreviousOnActivateProperty = serializedObject.FindProperty(nameof(ActionMapHandlerBase.DeactivatePreviousOnActivate));
        _activatePreviousOnDeactivateProperty = serializedObject.FindProperty(nameof(ActionMapHandlerBase.ActivatePreviousOnDeactivate));
        _useEventsProperty = serializedObject.FindProperty(nameof(ActionMapHandlerBase.UseEvents));
        _actionStartedEventsProperty = serializedObject.FindProperty(nameof(ActionMapHandlerBase.ActionStartedEvents));
        _actionPerformedEventsProperty = serializedObject.FindProperty(nameof(ActionMapHandlerBase.ActionPerformedEvents));
        _actionCanceledEventsPainter = serializedObject.FindProperty(nameof(ActionMapHandlerBase.ActionCanceledEvents));
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.UpdateIfRequiredOrScript();

        InformationMessageHeader = "Active handlers:";

        if (EditorApplication.isPlaying && !serializedObject.isEditingMultipleObjects)
        {
            foreach (var item in ActionMapHandlerBase.ActiveHandlers)
                InformationMessage += $"  • {item.ActionMap.name} ({item.gameObject.name})\n";

            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Activate Input"))
                    Target.ActivateInput();

                if (GUILayout.Button("Deactivate Input"))
                    Target.DeactivateInput();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
        }

        EditorGUILayout.PropertyField(_activateOnStartProperty);
        //EditorGUILayout.PropertyField(_deactivatePreviousOnActivateProperty);
        //EditorGUILayout.PropertyField(_activatePreviousOnDeactivateProperty);
        EditorGUILayout.PropertyField(_useEventsProperty);

        if (_useEventsProperty.boolValue)
        {
            EditorGUILayout.PropertyField(_actionStartedEventsProperty);
            EditorGUILayout.PropertyField(_actionPerformedEventsProperty);
            EditorGUILayout.PropertyField(_actionCanceledEventsPainter);
        }

        this.DrawAllProperties(new[] { nameof(ActionMapHandlerBase.ActivateOnStart),
                                       nameof(ActionMapHandlerBase.DeactivatePreviousOnActivate),
                                       nameof(ActionMapHandlerBase.ActivatePreviousOnDeactivate),
                                       nameof(ActionMapHandlerBase.UseEvents),
                                       nameof(ActionMapHandlerBase.ActionStartedEvents),
                                       nameof(ActionMapHandlerBase.ActionPerformedEvents),
                                       nameof(ActionMapHandlerBase.ActionCanceledEvents) });


        serializedObject.ApplyModifiedProperties();
    }
}