﻿using UnityEngine;
using UnityEditor;
using NightFramework.VFX;

// ========================
// Revision 2020.11.07
// ========================

[CustomPropertyDrawer(typeof(PaintDecal))]
public class PaintDecalPropertyDrawer : PropertyDrawer
{
    // ========================================================================================
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        var result = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
        var doPaint = property.FindPropertyRelative(nameof(PaintDecal.DoPaint));

        if (doPaint.boolValue && property.isExpanded)
        {
            var radius = property.FindPropertyRelative(nameof(PaintDecal.SpreadRadius));
            var textures = property.FindPropertyRelative(nameof(PaintDecal.Textures));

            result += EditorGUI.GetPropertyHeight(radius) + EditorGUIUtility.standardVerticalSpacing;
            result += EditorGUI.GetPropertyHeight(textures) + EditorGUIUtility.standardVerticalSpacing;
        }

        return result;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var r1 = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
        var r2 = new Rect(position.x, position.y, EditorGUIUtility.labelWidth, EditorGUIUtility.singleLineHeight);
        var r3 = new Rect(position.x + EditorGUIUtility.labelWidth + 2f, position.y, 15f, EditorGUIUtility.singleLineHeight);

        var doPaint = property.FindPropertyRelative(nameof(PaintDecal.DoPaint));

        if (!doPaint.boolValue)
        {
            EditorGUI.PropertyField(r1, doPaint, label);
        }
        else
        {
            property.isExpanded = EditorGUI.Foldout(r2, property.isExpanded, label, true);
            EditorGUI.PropertyField(r3, doPaint, GUIContent.none);

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;

                var r4 = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing, position.width, EditorGUIUtility.singleLineHeight);
                var r5 = new Rect(position.x, position.y + (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) * 2f, position.width, position.height - (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) * 2f);

                var p4 = property.FindPropertyRelative(nameof(PaintDecal.SpreadRadius));
                var p5 = property.FindPropertyRelative(nameof(PaintDecal.Textures));

                EditorGUI.PropertyField(r4, p4);
                EditorGUI.PropertyField(r5, p5, p5.isExpanded);
                EditorGUI.indentLevel--;
            }
        }
    }
}