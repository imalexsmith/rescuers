﻿using System;
using UnityEngine;
using UnityEditor;
using NightFramework.Inputs;

// ========================
// Revision 2021.01.29
// ========================

[CustomPropertyDrawer(typeof(NamedEvent))]
public class NamedEventPropertyDrawer : PropertyDrawer
{
    // ========================================================================================
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        var result = EditorGUI.GetPropertyHeight(property);
        if (property.isExpanded)
            result -= EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

        return result;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var h = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

        var r0 = new Rect(position.x, position.y, position.width, h);
        property.isExpanded = EditorGUI.Foldout(r0, property.isExpanded, label, true);

        if (property.isExpanded)
        {
            position.height -= h;
            position.y += h;

            var p1 = property.FindPropertyRelative(nameof(NamedEvent.Name));
            var p2 = property.FindPropertyRelative(nameof(NamedEvent.Guid));
            var p3 = property.FindPropertyRelative(nameof(NamedEvent.Event));

            var r1 = new Rect(position.x, position.y, position.width * 0.4f, h);
            var r2 = new Rect(position.x + position.width * 0.4f, position.y, position.width * 0.6f, h);
            var r3 = new Rect(position.x, position.y + h, position.width, position.height - h);

            EditorGUI.indentLevel++;
            EditorGUI.SelectableLabel(r1, $"Name: {p1.stringValue}", EditorStyles.miniBoldLabel);
            EditorGUI.SelectableLabel(r2, $"id: {p2.stringValue}", EditorStyles.miniBoldLabel);
            EditorGUI.PropertyField(r3, p3);
            EditorGUI.indentLevel--;
        }
    }
}