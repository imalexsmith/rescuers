﻿// ========================
// Revision 2021.01.28
// ========================

namespace NightFramework
{
    public static class Exceptions
    {
        public const string ApplicationSettingsMustBeUnique = "There cannot be more or less than single ApplicationSettings asset. Something went wrong...";

        public const string UnexpectedBehaviourWhileRandomSelection = "Unexpected behaviour during selection from RandomisedSet";
        public const string AdaptiveTextHolderExpected = "This component must inherit either ITMPTextHolder or IUGUITextHolder.";
        public const string RenderTextureNotAssigned = "Target render texture isn't assigned.";
    }
}