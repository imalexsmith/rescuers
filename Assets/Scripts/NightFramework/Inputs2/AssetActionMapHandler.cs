﻿using UnityEngine;                     
using UnityEngine.InputSystem;

// ========================
// Revision 2021.01.29
// ========================

namespace NightFramework.Inputs
{
    public class AssetActionMapHandler : ActionMapHandlerBase
    {
        // ========================================================================================
        [field: Space, SerializeField]
        public InputActionAsset Actions { get; private set; }
        
        [field: SerializeField, Delayed]
        public string ActionMapName { get; private set; }

        [field: SerializeField, ReadOnly]
        public string ActionMapGuid { get; private set; }
        
        [SerializeField, HideInInspectorIfNotDebug]
        private InputActionMap _actionMap;
        public override InputActionMap ActionMap
        {
            get
            {
                if (_actionMap == null || ActionMapName != _actionMap.name || ActionMapGuid != _actionMap.id.ToString())
                    LookUpActionMap();

                return _actionMap;
            }
        }


        // ========================================================================================
        protected override void OnValidate()
        {
            LookUpActionMap();

            base.OnValidate();
        }

        private void LookUpActionMap()
        {
            InputActionMap result = null;

            if (Actions != null)
            {
                if (!string.IsNullOrEmpty(ActionMapName))
                    result = Actions.FindActionMap(ActionMapName);

                if (result == null)
                    result = Actions.FindActionMap(ActionMapGuid);
            }

            if (result != null)
            {
                ActionMapName = result.name;
                ActionMapGuid = result.id.ToString();
                _actionMap = result;
            }
        }
    }
}