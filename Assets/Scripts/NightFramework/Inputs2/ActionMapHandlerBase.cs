﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UltEvents;

// ========================
// Revision 2021.02.01
// ========================

namespace NightFramework.Inputs
{
    [Serializable]
    public class NamedEvent 
    {
        [ReadOnly, Tooltip("Input Action Name")]
        public string Name;
        [ReadOnly, Tooltip("Input Action Id")]
        public string Guid;
        public UltEvent<InputAction.CallbackContext> Event;

        public NamedEvent(string name, string guid, UltEvent<InputAction.CallbackContext> e)
        {
            Name = name;
            Guid = guid;
            Event = e;
        }
    }


    public abstract class ActionMapHandlerBase : MonoBehaviour
    {
        // ========================================================================================
        // TODO: Implement ActivationStack functionality
        /*public static readonly Stack<ActionMapHandlerBase> ActivationStack = new Stack<ActionMapHandlerBase>();

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void OnBeforeSceneLoad()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private static void OnSceneLoaded(Scene scene, LoadSceneMode loadMode)
        {
            if (loadMode == LoadSceneMode.Single)
                ActivationStack.Clear();
        }*/

        public static readonly List<ActionMapHandlerBase> ActiveHandlers = new List<ActionMapHandlerBase>();

        public static void DeactivateAllHandlers()
        {
            for (int i = ActiveHandlers.Count - 1; i >= 0; i--)
                ActiveHandlers[i].DeactivateInput();
        }


        // ========================================================================================
        public abstract InputActionMap ActionMap { get; }

        public bool ActivateOnStart;
        public bool DeactivatePreviousOnActivate;
        public bool ActivatePreviousOnDeactivate;
        public bool UseEvents = true;

        public List<NamedEvent> ActionStartedEvents;
        public List<NamedEvent> ActionPerformedEvents;
        public List<NamedEvent> ActionCanceledEvents;


        // ========================================================================================
        public void ActivateInput()
        {
            if (!ActionMap.enabled)
            {
                ActionMap.Enable();

                if (UseEvents)
                    SubscribeEvents();

                /*if (ActivationStack.Count > 0 && DeactivatePreviousOnActivate)
                      ActivationStack.Pop().DeactivateInput();

                  if (ActivationStack.Count == 0 || ActivationStack.Peek() != this)
                      ActivationStack.Push(this);*/

                ActiveHandlers.Add(this);
            }
        }

        public void DeactivateInput()
        {
            if (ActionMap.enabled)
            {
                ActionMap.Disable();

                if (UseEvents)
                    UnsubscribeEvents();

                /*if (ActivationStack.Count > 0 && ActivationStack.Peek() == this)
                      ActivationStack.Pop();

                  if (ActivationStack.Count > 0 && ActivatePreviousOnDeactivate)
                      ActivationStack.Peek().ActivateInput();*/

                ActiveHandlers.Remove(this);
            }
        }

        protected void Start()
        {
            if (ActivateOnStart)
                ActivateInput();
        }

        protected virtual void OnValidate()
        {
            UpdateEvents();
        }

        private void SubscribeEvents()
        {
            foreach (var action in ActionMap.actions)
            {
                foreach (var e in ActionStartedEvents)
                {
                    if (action.name == e.Name || action.id.ToString() == e.Guid)
                    {
                        action.started += e.Event.Invoke;
                        break;
                    }
                }

                foreach (var e in ActionPerformedEvents)
                {
                    if (action.name == e.Name || action.id.ToString() == e.Guid)
                    {
                        action.performed += e.Event.Invoke;
                        break;
                    }
                }

                foreach (var e in ActionCanceledEvents)
                {
                    if (action.name == e.Name || action.id.ToString() == e.Guid)
                    {
                        action.canceled += e.Event.Invoke;
                        break;
                    }
                }
            }
        }

        private void UnsubscribeEvents()
        {
            foreach (var action in ActionMap.actions)
            {
                foreach (var e in ActionStartedEvents)
                {
                    if (action.name == e.Name || action.id.ToString() == e.Guid)
                    {
                        action.started -= e.Event.Invoke;
                        break;
                    }
                }

                foreach (var e in ActionPerformedEvents)
                {
                    if (action.name == e.Name || action.id.ToString() == e.Guid)
                    {
                        action.performed -= e.Event.Invoke;
                        break;
                    }
                }

                foreach (var e in ActionCanceledEvents)
                {
                    if (action.name == e.Name || action.id.ToString() == e.Guid)
                    {
                        action.canceled -= e.Event.Invoke;
                        break;
                    }
                }
            }
        }

        private void UpdateEvents()
        {
            UpdateEventsList(ActionStartedEvents, " Started");
            UpdateEventsList(ActionPerformedEvents, " Performed");
            UpdateEventsList(ActionCanceledEvents, " Canceled");
        }

        private void UpdateEventsList(List<NamedEvent> list, string eventTypeString)
        {
            if (ActionMap != null)
            {
                var removeFromInitialList = new List<NamedEvent>(list);
                foreach (var action in ActionMap.actions)
                {
                    var addToList = true;
                    for (var i = 0; i < list.Count; i++)
                    {
                        var e = list[i];
                        if (action.name == e.Name.Replace(eventTypeString, "") || action.id.ToString() == e.Guid)
                        {
                            removeFromInitialList.Remove(e);
                            e.Name = $"{action.name}{eventTypeString}";
                            e.Guid = action.id.ToString();
                            addToList = false;
                            break;
                        }
                    }

                    if (addToList)
                        list.Add(new NamedEvent($"{action.name}{eventTypeString}", action.id.ToString(), new UltEvent<InputAction.CallbackContext>()));
                }

                foreach (var e in removeFromInitialList)
                    list.Remove(e);
            }
        }
    }
}
