﻿using UnityEngine;
using UnityEngine.InputSystem;

// ========================
// Revision 2021.01.29
// ========================

namespace NightFramework.Inputs
{
    public class CustomActionMapHandler : ActionMapHandlerBase
    {
        // ========================================================================================
        [Space]
        [SerializeField]
        private InputActionMap _actionMap;
        public override InputActionMap ActionMap => _actionMap;


        // ========================================================================================
    }
}