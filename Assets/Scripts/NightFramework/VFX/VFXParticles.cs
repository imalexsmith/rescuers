﻿using System.Collections.Generic;
using UnityEngine;

// ========================
// Revision 2020.05.16
// ========================

namespace NightFramework.VFX
{
    [RequireComponent(typeof(ParticleSystem), typeof(ChangeRendererSorting))]
    public class VFXParticles : PoolableObject
    {
        // ========================================================================================
        public PaintDecal DecalPainter;

        [Space]
        public ParticleSystem CachedParticleSystem;
        public ChangeRendererSorting CachedChangeRendererSorting;

        private List<ParticleCollisionEvent> _collisionEvents = new List<ParticleCollisionEvent>();


        // ========================================================================================
        public void Setup(GameObject parent)
        {
            transform.SetParent(parent.transform);
            var r = parent.GetComponentInParent<Renderer>();
            if (r)
            {
                CachedChangeRendererSorting.TargetRenderer = r;
                CachedChangeRendererSorting.UpdateSorting();
            }
        }

        public override void WakeUp()
        {
            base.WakeUp();

            CachedParticleSystem.Play();
        }

        protected void Reset()
        {
            CachedParticleSystem = GetComponent<ParticleSystem>();
            CachedChangeRendererSorting = GetComponent<ChangeRendererSorting>();
        }

        protected void Awake()
        {
            if (!CachedParticleSystem)
                CachedParticleSystem = GetComponent<ParticleSystem>();
            if (!CachedChangeRendererSorting)
                CachedChangeRendererSorting = GetComponent<ChangeRendererSorting>();
        }

        protected void OnDisable()
        {
            if (IsAwakened)
                ReturnToPool();
        }

        protected void OnParticleCollision(GameObject other)
        {
            if (DecalPainter.DoPaint)
            {
                var dCanvas = other.GetComponent<DecalsCanvas>();
                if (dCanvas)
                {
                    var numCollisionEvents = CachedParticleSystem.GetCollisionEvents(other, _collisionEvents);

                    for (int i = 0; i < numCollisionEvents; i++)
                        DecalPainter.Paint(dCanvas, _collisionEvents[i].intersection);
                }
            }
        }

        protected void OnParticleSystemStopped()
        {
            ReturnToPool();
        }
    }
}
