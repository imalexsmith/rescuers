﻿using UnityEngine;
using MyBox;

// ========================
// Revision 2020.03.31
// ========================

namespace NightFramework.VFX
{
    public class RenderTextureResize : MonoBehaviour
    {
        public enum ScaleModes
        {
            x1 = 1,
            x2 = 2,
            x4 = 4,
            x8 = 8,
            x16 = 16
        }


        // ========================================================================================
        public Camera GrabCamera;
        public RenderTexture Target;
        public bool UseScreenSize = true;
        [ConditionalField(nameof(UseScreenSize), true)]
        public int Width;
        [ConditionalField(nameof(UseScreenSize), true)]
        public int Height;
        public ScaleModes DownScaleMode;
        public ScaleModes UpScaleMode;


        // ========================================================================================
        public void DownScale()
        {
            if (!Target)
            {
                Debug.LogError(Exceptions.RenderTextureNotAssigned);
                return;
            }

            if (Target.IsCreated())
                Target.Release();

#if UNITY_EDITOR
            Target.width = (UseScreenSize ? Screen.width : Width) / (int)DownScaleMode;
            Target.height = (UseScreenSize ? Screen.height : Height) / (int)DownScaleMode;
#else
            Target.width = (UseScreenSize ? Screen.currentResolution.width : Width) / (int)DownScaleMode;
            Target.height = (UseScreenSize ? Screen.currentResolution.height : Height) / (int)DownScaleMode;
#endif

            Target.Create();

            if (GrabCamera)
            {
                var oldRt = GrabCamera.activeTexture;
                GrabCamera.targetTexture = Target;
                GrabCamera.Render();
                GrabCamera.targetTexture = oldRt;
            }
        }

        public void UpScale()
        {
            if (!Target)
            {
                Debug.LogError(Exceptions.RenderTextureNotAssigned);
                return;
            }

            if (Target.IsCreated())
                Target.Release();

            Target.width = (UseScreenSize ? Screen.width : Width) * (int)UpScaleMode;
            Target.height = (UseScreenSize ? Screen.height : Height) * (int)UpScaleMode;

            Target.Create();

            if (GrabCamera)
            {
                var oldRt = GrabCamera.activeTexture;
                GrabCamera.targetTexture = Target;
                GrabCamera.Render();
                GrabCamera.targetTexture = oldRt;
            }
        }
    }
}