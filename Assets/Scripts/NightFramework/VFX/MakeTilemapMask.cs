﻿using UnityEngine;
using UnityEngine.Tilemaps;

// ========================
// Revision 2020.05.18
// ========================

namespace NightFramework.VFX
{
    [RequireComponent(typeof(SpriteMask))]
    public class MakeTilemapMask : MonoBehaviour
    {
        // ========================================================================================
        public Tilemap TargetTilemap;
        public int PixelsPerTile = 32;

        [Space]
        public SpriteMask CachedSpriteMask;

        private int _maskWidth;
        private int _maskHeight;
        private Texture2D _maskTex;
        private Sprite _maskSprite;

        private Vector3Int _tIndex;
        private Sprite _tSprite;
        private Texture2D _tTexEmpty;
        private Texture2D _tTexInitial;
        private RenderTexture _tTexResized;
        private int _tX;
        private int _tY;
        private int _tW;
        private int _tH;
        private int _dstX;
        private int _dstY;


        // ========================================================================================
        protected void Reset()
        {
            CachedSpriteMask = GetComponent<SpriteMask>();
        }

        protected void Awake()
        {
            if (!CachedSpriteMask)
                CachedSpriteMask = GetComponent<SpriteMask>();
        }

        protected void Start()
        {
            Setup();
        }

        protected void OnDestroy()
        {
            Destroy(_maskTex);
            Destroy(_maskSprite);
            Destroy(_tTexEmpty);
            Destroy(_tTexInitial);
        }

        private void Setup()
        {
            if (TargetTilemap)
            {
                CreateMask();

                CachedSpriteMask.sprite = _maskSprite;
                CachedSpriteMask.transform.localPosition = new Vector3(TargetTilemap.cellBounds.center.x,
                                                                      TargetTilemap.cellBounds.center.y,
                                                                      0f);
            }
        }

        private void CreateMask()
        {
            if (TargetTilemap)
            {
                _maskWidth = TargetTilemap.size.x * PixelsPerTile;
                _maskHeight = TargetTilemap.size.y * PixelsPerTile;
                _maskTex = new Texture2D(_maskWidth, _maskHeight, TextureFormat.ARGB32, false);
                _tTexEmpty = new Texture2D(PixelsPerTile, PixelsPerTile, TextureFormat.ARGB32, false);
                var colors = new Color[PixelsPerTile * PixelsPerTile];
                for (int i = 0; i < colors.Length; i++)
                    colors[i] = Color.clear;
                _tTexEmpty.SetPixels(colors);
                _tTexEmpty.Apply();

                for (int j = TargetTilemap.origin.y; j < TargetTilemap.origin.y + TargetTilemap.size.y; j++)
                {
                    for (int i = TargetTilemap.origin.x; i < TargetTilemap.origin.x + TargetTilemap.size.x; i++)
                    {
                        _tIndex = new Vector3Int(i, j, 0);
                        _tSprite = TargetTilemap.GetSprite(_tIndex);
                        _dstX = (i - TargetTilemap.origin.x) * PixelsPerTile;
                        _dstY = (j - TargetTilemap.origin.y) * PixelsPerTile;

                        if (_tSprite != null)
                        {
                            _tX = (int)_tSprite.rect.x;
                            _tY = (int)_tSprite.rect.y;
                            _tW = (int)_tSprite.rect.width;
                            _tH = (int)_tSprite.rect.height;

                            _tTexInitial = new Texture2D(_tW, _tH, _tSprite.texture.format, false);
                            Graphics.CopyTexture(_tSprite.texture, 0, 0, _tX, _tY, _tW, _tH, _tTexInitial, 0, 0, 0, 0);

                            _tTexResized = RenderTexture.GetTemporary(PixelsPerTile, PixelsPerTile, 0, RenderTextureFormat.ARGB32);
                            Graphics.Blit(_tTexInitial, _tTexResized);

                            Graphics.CopyTexture(_tTexResized, 0, 0, 0, 0, PixelsPerTile, PixelsPerTile, _maskTex, 0, 0, _dstX, _dstY);

                            RenderTexture.ReleaseTemporary(_tTexResized);
                        }
                        else
                        {
                            Graphics.CopyTexture(_tTexEmpty, 0, 0, 0, 0, PixelsPerTile, PixelsPerTile, _maskTex, 0, 0, _dstX, _dstY);
                        }
                    }
                }

                _maskSprite = Sprite.Create(_maskTex, new Rect(0f, 0f, _maskTex.width, _maskTex.height), new Vector2(0.5f, 0.5f), PixelsPerTile);
            }
        }
    }
}
