﻿using UnityEngine;

// ========================
// Revision 2020.03.31
// ========================

namespace NightFramework.VFX
{
    public class RenderTextureBlur : MonoBehaviour
    {
        // ========================================================================================
        public Camera GrabCamera;
        public RenderTexture Target;
        public Material BlurMaterial;


        // ========================================================================================
        public void Blur()
        {
            if (!Target)
            {
                Debug.LogError(Exceptions.RenderTextureNotAssigned);
                return;
            }

            var tmpRt = RenderTexture.GetTemporary(Target.descriptor);

            if (GrabCamera)
            {
                var oldRt = GrabCamera.activeTexture;
                GrabCamera.targetTexture = tmpRt;
                GrabCamera.Render();
                GrabCamera.targetTexture = oldRt;
            }
            else
            {
                Graphics.Blit(Target, tmpRt);
            }

            if (BlurMaterial)
                Graphics.Blit(tmpRt, Target, BlurMaterial);
            else
                Graphics.Blit(tmpRt, Target);

            RenderTexture.ReleaseTemporary(tmpRt);
        }
    }
}