﻿using System.Collections.Generic;
using UnityEngine;

// ========================
// Revision 2020.11.07
// ========================

namespace NightFramework.VFX
{
    public struct DecalEntry
    {
        public Texture Tex;
        public Vector2 WPos;
    }


    public class DecalsCanvas : MonoBehaviour
    {
        // ========================================================================================
        [Tooltip("Size of the canvas in world-space units")]
        public Vector2Int Size;
        public int PixelsPerUnit = 32;
        public FilterMode TextureFilterMode;
        public SpriteRenderer TargetSpriteRenderer;
        public Material BlitMaterial;

        private Queue<DecalEntry> _entries;
        private bool _entriesChanged;
        private DecalEntry _currentDecal;

        private int _canvasWidth;
        private int _canvasHeight;
        private RenderTexture _decalRendTex;
        private RenderTexture _combinedRendTex;
        private Texture2D _canvasTex;
        private Sprite _canvasSprite;

        private Vector3 _decalLocPos;
        private int _decalPixPosX;
        private int _decalPixPosY;


        // ========================================================================================
        public void Paint(DecalEntry decal)
        {
            _entries.Enqueue(decal);
            _entriesChanged = true;
        }

        protected virtual void Awake()
        {
            _entries = new Queue<DecalEntry>();

            _canvasWidth = Size.x * PixelsPerUnit;
            _canvasHeight = Size.y * PixelsPerUnit;
        }

        protected void Start()
        {
            _decalRendTex = new RenderTexture(_canvasWidth, _canvasHeight, 0, RenderTextureFormat.ARGB32);
            _combinedRendTex = new RenderTexture(_canvasWidth, _canvasHeight, 0, RenderTextureFormat.ARGB32);
            _canvasTex = new Texture2D(_canvasWidth, _canvasHeight, TextureFormat.ARGB32, false);
            _canvasTex.filterMode = TextureFilterMode;
            var colors = new Color[_canvasWidth * _canvasHeight];
            for (int i = 0; i < colors.Length; i++)
                colors[i] = Color.clear;
            _canvasTex.SetPixels(colors);
            _canvasTex.Apply();

            _canvasSprite = Sprite.Create(_canvasTex, new Rect(0f, 0f, _canvasWidth, _canvasHeight), new Vector2(0.5f, 0.5f), PixelsPerUnit);

            if (TargetSpriteRenderer)
                TargetSpriteRenderer.sprite = _canvasSprite;
        }

        protected void LateUpdate()
        {
            if (!_entriesChanged || !TargetSpriteRenderer)
                return;

            Graphics.Blit(_canvasTex, _combinedRendTex);

            while (_entries.Count > 0)
            {
                _currentDecal = _entries.Dequeue();
                _decalLocPos = TargetSpriteRenderer.transform.InverseTransformPoint(_currentDecal.WPos);
                _decalPixPosX = Mathf.Clamp((int)(_decalLocPos.x * PixelsPerUnit + _canvasWidth / 2), 0, _canvasWidth);
                _decalPixPosY = Mathf.Clamp((int)(_decalLocPos.y * PixelsPerUnit + _canvasHeight / 2), 0, _canvasHeight);

                DrawDecalTexute(_currentDecal.Tex, _decalPixPosX, _decalPixPosY);
            }

            Graphics.CopyTexture(_combinedRendTex, _canvasTex);

            _entriesChanged = false;
        }

        protected void OnDestroy()
        {
            _decalRendTex.DiscardContents();
            _decalRendTex.Release();
            Destroy(_decalRendTex);

            _combinedRendTex.DiscardContents();
            _combinedRendTex.Release();
            Destroy(_combinedRendTex);

            Destroy(_canvasTex);
            Destroy(_canvasSprite);
        }

        private void DrawDecalTexute(Texture tex, int pixelPosX, int pixelPosY)
        {
            if (tex == null)
                return;

            var srcX = pixelPosX - tex.width / 2;
            srcX = srcX < 0 ? -srcX : 0;

            var srcY = pixelPosY - tex.height / 2;
            srcY = srcY < 0 ? -srcY : 0;

            var srcWidth = pixelPosX + tex.width / 2;
            srcWidth = srcWidth > _canvasWidth ? srcWidth - _canvasWidth : tex.width;
            srcWidth -= srcX;

            var srcHeight = pixelPosY + tex.height / 2;
            srcHeight = srcHeight > _canvasHeight ? srcHeight - _canvasHeight : tex.height;
            srcHeight -= srcY;

            var dstX = Mathf.Max(pixelPosX - tex.width / 2, 0);
            var dstY = Mathf.Max(pixelPosY - tex.height / 2, 0);

            Graphics.CopyTexture(tex, 0, 0, srcX, srcY, srcWidth, srcHeight, _decalRendTex, 0, 0, dstX, dstY);
            
            if (BlitMaterial)
                Graphics.Blit(_decalRendTex, _combinedRendTex, BlitMaterial);
            else
                Graphics.Blit(_decalRendTex, _combinedRendTex);
        }
    }
}
