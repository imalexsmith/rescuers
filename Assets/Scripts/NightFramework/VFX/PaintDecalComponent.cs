﻿using System;
using UnityEngine;

// ========================
// Revision 2020.11.07
// ========================

namespace NightFramework.VFX
{
    public sealed class PaintDecalComponent : MonoBehaviour
    {
        public PaintDecal Painter;
    }


    [Serializable]
    public class PaintDecal
    {
        // ========================================================================================
        public bool DoPaint = true;
        public Vector2 SpreadRadius = Vector2.zero;
        public RandomisedSet<Texture2D> Textures;


        // ========================================================================================
        public void Paint(DecalsCanvas host, Vector3 worldPosition)
        {
            if (!DoPaint)
                return;

            var ts = Textures.SelectRandomValues();
            foreach (var t in ts)
                host.Paint(new DecalEntry { Tex = t, WPos = (Vector2)worldPosition + UnityEngine.Random.insideUnitCircle * SpreadRadius });
        }
    }
}
