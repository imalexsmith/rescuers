﻿using System;
using System.Collections.Generic;

// ========================
// Revision 2021.02.02
// ========================

namespace TheGame
{
    public enum FallingObjectKeys
    { 
        Citizen = 0,
        Trash = 1
    }

    public enum ContinuousEffectKeys
    {
        Poison = 0,
        Burn = 1,
        Freeze = 2,
        Blind = 3
    }


    public static class Enums
    {
        public static void RecreateList<T>(ref List<T> list, Type enumType)
        {
            var values = Enum.GetValues(enumType);
            var maxIndex = (int)values.GetValue(values.Length - 1);
            list = new List<T>(maxIndex);

            for (int i = 0; i <= maxIndex; i++)
                list.Add(default);
        }
    }
}