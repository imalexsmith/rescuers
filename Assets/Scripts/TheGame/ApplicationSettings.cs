﻿using System;
using UnityEngine;
using NightFramework;

// ========================
// Revision 2020.11.06
// ========================

namespace TheGame
{
    public class ApplicationSettings : ApplicationSettingsBase
    {
        // ========================================================================================
        public static new ApplicationSettings Instance => ApplicationSettingsBase.Instance as ApplicationSettings;


        // ========================================================================================
        [Header("Properties depend on game")]
        [SerializeField]
        private float _demoProperty;
        public float DemoProperty
        {
            get
            {
                if (!IsLoaded)
                    LoadFromFile();

                return _demoProperty;
            }
            set
            {
                if (value != _demoProperty)
                {
                    var oldValue = _demoProperty;
                    _demoProperty = value;

                    OnDemoPropertyChange?.Invoke(oldValue, value);
                }
            }
        }

        public event Action<float, float> OnDemoPropertyChange;


        // ========================================================================================
    }
}
