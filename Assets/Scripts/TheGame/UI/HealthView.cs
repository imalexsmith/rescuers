using UnityEngine;
using TMPro;

// ========================
// Revision 2021.02.06
// ========================

namespace TheGame
{
    public class HealthView : MonoBehaviour
    {
        // ========================================================================================
        public TMP_Text ValueText;


        // ========================================================================================
        public void Refresh()
        {
            ValueText.text = LevelManager.Instance.Health.ToString();
        }

        protected void Start()
        {
            LevelManager.Instance.OnHealthChanged += Refresh;
            Refresh();
        }
    }
}