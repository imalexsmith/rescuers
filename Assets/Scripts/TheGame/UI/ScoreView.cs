using UnityEngine;
using TMPro;

// ========================
// Revision 2021.02.06
// ========================

namespace TheGame
{
    public class ScoreView : MonoBehaviour
    {
        // ========================================================================================
        public TMP_Text ValueText;


        // ========================================================================================
        public void Refresh()
        {
            ValueText.text = $"{LevelManager.Instance.Score}/{LevelManager.Instance.ScoreLimit}";
        }

        protected void Start()
        {
            LevelManager.Instance.OnScoreChanged += Refresh;
            LevelManager.Instance.OnCurrentBuildingChanged += Refresh;
            Refresh();
        }
    }
}