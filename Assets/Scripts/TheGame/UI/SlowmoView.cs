using UnityEngine;
using UnityEngine.UI;

// ========================
// Revision 2021.02.06
// ========================

namespace TheGame
{
    public class SlowmoView : MonoBehaviour
    {
        // ========================================================================================
        public Image ValueImage;


        // ========================================================================================
        protected void Update()
        {
            ValueImage.fillAmount = Slowmo.Instance.CurrentAmount;
        }
    }
}