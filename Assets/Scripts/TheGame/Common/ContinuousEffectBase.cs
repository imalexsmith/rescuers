﻿using UnityEngine;
using NightFramework;
using UltEvents;

// ========================
// Revision 2020.11.08
// ========================

namespace TheGame
{
    public enum EffectModes
    {
        Override = 0,
        New = 1,
        Accumulate = 2
    }

    public abstract class ContinuousEffectBase : PoolableObject
    {
        // ========================================================================================
        public ContinuousEffectKeys EffectGroup;
        public EffectModes Mode;
        public SuperTimer Timer;

        [Space]
        public UltEvent OnImpact = new UltEvent();

        public float DefaultDuration { get; private set; }


        // ========================================================================================
        public abstract void Setup(GameObject source, GameObject target);

        public virtual void Impact()
        {
            OnImpact.Invoke();
        }

        protected virtual void Awake()
        {
            DefaultDuration = Timer.Duration;

            Timer.NeedOnComplete = true;
            Timer.OnComplete += ReturnToPool;

            Timer.NeedOnStop = true;
            Timer.OnStop += ReturnToPool;
        }

        protected void Update()
        {
            Timer.Update();
        }
    }
}