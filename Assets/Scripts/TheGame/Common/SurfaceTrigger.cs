﻿using UnityEngine;
using NightFramework;

// ========================
// Revision 2020.04.15
// ========================

namespace TheGame
{
    [RequireComponent(typeof(Collider2D))]
    public class SurfaceTrigger : MonoBehaviour
    {
        // ========================================================================================
        public LayerMask SurfaceLayerMask;

        [Space]
        public Collider2D CachedCollider2D;

        public bool InContact { get; protected set; }


        // ========================================================================================
        protected void Reset()
        {
            CachedCollider2D = GetComponent<Collider2D>();
            CachedCollider2D.isTrigger = true;
        }

        protected void Awake()
        {
            if (!CachedCollider2D)
                CachedCollider2D = GetComponent<Collider2D>();
        }

        protected void OnTriggerEnter2D(Collider2D other)
        {
            if (SurfaceLayerMask.Contains(other.gameObject))
                InContact = true;
        }

        protected void OnTriggerExit2D(Collider2D other)
        {
            InContact = CachedCollider2D.IsTouchingLayers(SurfaceLayerMask.value);
        }
    }
}