﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using NightFramework;
using NightFramework.VFX;

// ========================
// Revision 2020.11.08
// ========================

namespace TheGame
{
    public class ContinuousEffectsHolder : MonoBehaviour
    {
        // ========================================================================================
        public Transform EffectsAnchor;
        public List<Reaction<ContinuousEffectKeys>> EffectImpactReactions = new List<Reaction<ContinuousEffectKeys>>();
        [ReadOnly]
        public List<ContinuousEffectBase> Effects = new List<ContinuousEffectBase>();


        // ========================================================================================
        public void AddEffect(ContinuousEffectBase prefab, GameObject source)
        {
            ContinuousEffectBase effect = null;
            switch (prefab.Mode)
            {
                case EffectModes.New:
                    AddNew(prefab, source);
                    break;

                case EffectModes.Override:
                    if (Effects.Count > 0)
                        effect = Effects.First(x => x.EffectGroup == prefab.EffectGroup);

                    if (effect != null)
                        effect.Timer.Stop();

                    AddNew(prefab, source);
                    break;

                case EffectModes.Accumulate:
                    if (Effects.Count > 0)
                        effect = Effects.First(x => x.EffectGroup == prefab.EffectGroup);

                    if (effect != null)
                        effect.Timer.Duration += effect.DefaultDuration;
                    else
                        AddNew(prefab, source);
                    break;

                default:
                    return;
            }
        }

        public void RemoveAllEffectsOfGroup(ContinuousEffectKeys group)
        {
            if (Effects.Count > 0)
            {
                var effects = Effects.Where(x => x.EffectGroup == group);
                foreach (var effect in effects)
                    effect.Timer.Stop();
            }
        }

        public void RemoveAllEffects()
        {
            for (int i = Effects.Count - 1; i >= 0; i--)
                Effects[i].Timer.Stop();
        }

        protected void Awake()
        {
            if (EffectsAnchor == null)
                EffectsAnchor = transform;
        }

        // TODO: Eh, I know, this is a terrible practice to use a local functions. Gonna remake this in the future. Maybe.
        private void AddNew(ContinuousEffectBase prefab, GameObject source)
        {
            if (MassSpawner.Instance != null)
            {
                var newEffect = MassSpawner.Instance.Spawn(prefab);
                newEffect.transform.SetPositionAndRotation(EffectsAnchor.position + prefab.transform.position, prefab.transform.rotation);
                newEffect.transform.SetParent(EffectsAnchor);
                newEffect.Timer.Duration = newEffect.DefaultDuration;

                newEffect.OnImpact += checkReaction;
                void checkReaction()
                {
                    foreach (var react in EffectImpactReactions)
                    {
                        if (react.Value == newEffect.EffectGroup)
                        {
                            if (react.VFXPrefab != null)
                            {
                                var vfx = MassSpawner.Instance.Spawn(react.VFXPrefab);
                                vfx.transform.SetPositionAndRotation(newEffect.transform.position, newEffect.transform.rotation);
                                vfx.Setup(newEffect.gameObject);
                            }

                            react.Event.Invoke();
                        }
                    }
                }

                newEffect.OnSleep += release;
                void release()
                {
                    Effects.Remove(newEffect);
                    newEffect.OnSleep -= release;
                    newEffect.OnImpact -= checkReaction;
                }

                Effects.Add(newEffect);

                newEffect.Setup(source, gameObject);
                newEffect.Timer.Start();
            }
            else
                throw new UnityException(Exceptions.MassSpawnerNotFound);
        }
    }
}