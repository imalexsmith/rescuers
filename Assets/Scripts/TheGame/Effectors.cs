﻿using System;
using NightFramework;

// ========================
// Revision 2020.11.07
// ========================

namespace TheGame
{
    [Serializable]
    public class DemoEffector
    {
        public PropertyEffectInt Health;
        public PropertyEffectFloat MoveSpeed;
        public PropertyEffectBool Invulnerability;
    }
}