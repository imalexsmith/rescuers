﻿// ========================
// Revision 2021.01.28
// ========================

namespace TheGame
{
    public static class Exceptions
    {
        public const string GameDataLibraryMustBeUnique = "There cannot be more or less than single GameDataLibrary asset. Something went wrong...";
        public const string PersistentDataStorageMustBeUnique = "There cannot be more or less than single PersistentDataStorage asset. Something went wrong...";
        public const string CraftingManagerMustBeUnique = "There cannot be more or less than single CraftingManager asset. Something went wrong...";

        public const string LootItemSelectionFailed = "Loot item selection failed!";
        public const string InvalidAmmoIndex = "Invalid ammo index or index out of range.";
        public const string MassSpawnerNotFound = "Mass spawner isn't on scene! Can't spawn object.";
        public const string RenderTextureNotAssigned = "Target render texture doesn't assigned.";
    }
}
