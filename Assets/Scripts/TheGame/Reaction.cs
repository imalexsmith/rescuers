﻿using UltEvents;
using NightFramework.VFX;
using System;

// ========================
// Revision 2020.11.08
// ========================

namespace TheGame
{
    [Serializable]
    public class Reaction<T>
    {
        public T Value;
        public VFXParticles VFXPrefab;
        public UltEvent Event;
    }


    [Serializable]
    public class Reaction<T1, T2>
    {
        public T1 Value1;
        public T2 Value2;
        public VFXParticles VFXPrefab;
        public UltEvent Event;
    }
}
