﻿using System;
using System.Collections.Generic;
using UnityEngine;

// ========================
// Revision 2021.02.02
// ========================

namespace TheGame
{
    public class GameDataLibrary : ScriptableObject
    {
        // ========================================================================================
        private static GameDataLibrary _instance;
        public static GameDataLibrary Instance
        {
            [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
            get
            {
                if (_instance == null)
                {
                    var assets = Resources.FindObjectsOfTypeAll<GameDataLibrary>();
                    if (assets.Length == 1)
                        _instance = assets[0];
                    else
                        throw new UnityException(Exceptions.GameDataLibraryMustBeUnique);
                }

                return _instance;
            }
        }


        // ========================================================================================


        // ========================================================================================
        public GameDataLibrary()
        {
           
        }
    }
}