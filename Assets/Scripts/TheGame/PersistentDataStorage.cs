﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using NightFramework;

// ========================
// Revision 2021.02.02
// ========================

namespace TheGame
{
    public class PersistentDataStorage : ScriptableObject, ISaveLoadFile
    {
        public const string FileName = "perstData.txt";


        // ========================================================================================
        private static PersistentDataStorage _instance;
        public static PersistentDataStorage Instance
        {
            [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
            get
            {
                if (_instance == null)
                {
                    var assets = Resources.FindObjectsOfTypeAll<PersistentDataStorage>();
                    if (assets.Length == 1)
                        _instance = assets[0];
                    else
                        throw new UnityException(Exceptions.PersistentDataStorageMustBeUnique);
                }

                return _instance;
            }
        }


        // ========================================================================================
        public string FileFullPath => Path.Combine(Application.persistentDataPath, FileName);


        // ========================================================================================
        public PersistentDataStorage()
        {
        }

        public void SaveToFile(bool compressed = true)
        {
            var dat = JsonUtility.ToJson(this, !compressed);
            File.WriteAllText(FileFullPath, dat, Encoding.UTF8);
        }

        public bool LoadFromFile()
        {
            if (File.Exists(FileFullPath))
            {
                var text = File.ReadAllText(FileFullPath, Encoding.UTF8);
                JsonUtility.FromJsonOverwrite(text, this);
                return true;
            }

            return false;
        }
    }
}