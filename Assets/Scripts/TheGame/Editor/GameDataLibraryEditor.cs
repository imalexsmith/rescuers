﻿using UnityEditor;
using UnityEngine;
using TheGame;

// ========================
// Revision 2020.11.09
// ========================

[CustomEditor(typeof(GameDataLibrary))]
public class GameDataLibraryEditor : CustomEditorBase<GameDataLibrary>
{
    // ========================================================================================
    [MenuItem("TheGame/GameDataDB", priority = 11)]
    private static void CreateOrSelectAsset()
    {
        EditorExtend.CreateUniqueScriptableObjectOrSelect<GameDataLibrary>(Exceptions.GameDataLibraryMustBeUnique);
    }

    [InitializeOnLoadMethod]
    private static void LoadAsset()
    {
        EditorExtend.LoadUniqueScriptableObject<GameDataLibrary>();
    }


    // ========================================================================================
    private SerializedProperty _continuousEffectsProperty;
    private SerializedProperty _damagesProperty;
    private SerializedProperty _ammosProperty;
    private SerializedProperty _resourcesProperty;
    private SerializedProperty _craftingsProperty;


    // ========================================================================================
    protected override void OnEnable()
    {
        base.OnEnable();

        _continuousEffectsProperty = serializedObject.FindProperty("_continuousEffects");
        _damagesProperty = serializedObject.FindProperty("_damages");
        _ammosProperty = serializedObject.FindProperty("_ammos");
        _resourcesProperty = serializedObject.FindProperty("_resources");
        _craftingsProperty = serializedObject.FindProperty("_craftings");

        ShowMessage = false;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.UpdateIfRequiredOrScript();

        /*this.DrawEnumsBlock<ContinuousEffectKeys>(_continuousEffectsProperty);
        this.DrawEnumsBlock<DamageKeys>(_damagesProperty);
        this.DrawEnumsBlock<AmmoKeys>(_ammosProperty);
        this.DrawEnumsBlock<ResourceKeys>(_resourcesProperty);
        this.DrawEnumsBlock<CraftingKeys>(_craftingsProperty);

        this.DrawAllProperties(new[] { "_continuousEffects", "_damages", "_ammos", "_resources", "_craftings" });*/
        this.DrawAllProperties();

        serializedObject.ApplyModifiedProperties();
    }
}
