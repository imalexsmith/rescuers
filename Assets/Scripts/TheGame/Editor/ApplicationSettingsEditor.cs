﻿using UnityEditor;
using TheGame;

// ========================
// Revision 2021.01.28
// ========================

[CustomEditor(typeof(ApplicationSettings), true)]
public class ApplicationSettingsEditor : CustomEditorBase<ApplicationSettings>
{
    // ========================================================================================
    [MenuItem("TheGame/ApplicationSettings", priority = 10)]
    private static void CreateOrSelectAsset()
    {
        EditorExtend.CreateUniqueScriptableObjectOrSelect<ApplicationSettings>(NightFramework.Exceptions.ApplicationSettingsMustBeUnique);
    }

    [InitializeOnLoadMethod]
    private static void LoadAsset()
    {
        EditorExtend.LoadUniqueScriptableObject<ApplicationSettings>();
    }


    // ========================================================================================
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.UpdateIfRequiredOrScript();

        WarningMessage = "<b>Do not forget to press \"Reset\" before publishing!</b>";

        this.DrawAllProperties();
        this.DrawSaveLoadFileBlock();

        serializedObject.ApplyModifiedProperties();
    }
}
