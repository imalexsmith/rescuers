﻿using UnityEditor;
using UnityEngine;
using TheGame;

// ========================
// Revision 2020.11.09
// ========================

[CustomEditor(typeof(PersistentDataStorage))]
public class PersistentDataStorageEditor : CustomEditorBase<PersistentDataStorage>
{
    // ========================================================================================
    [MenuItem("TheGame/PersistentDataStorage", priority = 12)]
    private static void CreateOrSelectAsset()
    {
        EditorExtend.CreateUniqueScriptableObjectOrSelect<PersistentDataStorage>(Exceptions.PersistentDataStorageMustBeUnique);
    }

    [InitializeOnLoadMethod]
    private static void LoadAsset()
    {
        EditorExtend.LoadUniqueScriptableObject<PersistentDataStorage>();
    }


    // ========================================================================================
    private SerializedProperty _ammosProperty;
    private SerializedProperty _resourcesProperty;
    private SerializedProperty _craftingsProperty;


    // ========================================================================================
    protected override void OnEnable()
    {
        base.OnEnable();

        _ammosProperty = serializedObject.FindProperty("_ammos");
        _resourcesProperty = serializedObject.FindProperty("_resources");
        _craftingsProperty = serializedObject.FindProperty("_craftings");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.UpdateIfRequiredOrScript();

        WarningMessage = "<b>Do not forget to press \"Reset\" before publishing!</b>";

        /*this.DrawAllProperties(new[] { "_ammos", "_resources", "_craftings" });

        this.DrawEnumsBlock<AmmoKeys>(_ammosProperty);
        this.DrawEnumsBlock<ResourceKeys>(_resourcesProperty);
        this.DrawEnumsBlock<CraftingKeys>(_craftingsProperty);*/

        this.DrawAllProperties();

        this.DrawSaveLoadFileBlock();

        serializedObject.ApplyModifiedProperties();
    }
}
