﻿using UnityEditor;
using UnityEngine;
using TheGame;

// ========================
// Revision 2020.11.10
// ========================

/*[CustomPropertyDrawer(typeof(ResourceRequirement))]
public class ResourceRequirementPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var w1 = EditorGUI.indentLevel * 15f;
        var w2 = position.width / 3f;
        var w3 = w1 / 3f;

        var r1 = new Rect(position.x, position.y, w2 + w1 - w3, position.height);
        var r2 = new Rect(position.x + w2 - w3, position.y, w2 + w1 - w3, position.height);
        var r3 = new Rect(position.x + (w2 - w3) * 2f, position.y, w2 + w1 - w3, position.height);

        EditorGUI.PropertyField(r1, property.FindPropertyRelative(nameof(ResourceRequirement.ResourceType)), GUIContent.none);
        EditorGUI.PropertyField(r2, property.FindPropertyRelative(nameof(ResourceRequirement.Condition)), GUIContent.none);
        EditorGUI.PropertyField(r3, property.FindPropertyRelative(nameof(ResourceRequirement.Value)), GUIContent.none);

        property.isExpanded = true;
        label = GUIContent.none;
    }
}

[CustomPropertyDrawer(typeof(TierRequirement))]
public class UnlockedTierRequirementPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var w1 = EditorGUI.indentLevel * 15f;
        var w2 = position.width / 3f;
        var w3 = w1 / 3f;

        var r1 = new Rect(position.x, position.y, w2 + w1 - w3, position.height);
        var r2 = new Rect(position.x + w2 - w3, position.y, w2 + w1 - w3, position.height);
        var r3 = new Rect(position.x + (w2 - w3) * 2f, position.y, w2 + w1 - w3, position.height);

        EditorGUI.PropertyField(r1, property.FindPropertyRelative(nameof(TierRequirement.CraftingType)), GUIContent.none);
        EditorGUI.PropertyField(r2, property.FindPropertyRelative(nameof(TierRequirement.Tier)), GUIContent.none);
        EditorGUI.PropertyField(r3, property.FindPropertyRelative(nameof(TierRequirement.Value)), GUIContent.none);

        property.isExpanded = true;
        label = GUIContent.none;
    }
}*/