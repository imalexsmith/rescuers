﻿using UnityEngine;
using UnityEngine.InputSystem;
using UltEvents;
using MyBox;

// ========================
// Revision 2021.02.08
// ========================

namespace TheGame
{
    public class Trampoline : NightFramework.Singleton<Trampoline>
    {
        // ========================================================================================
        public UltEvent OnCitizenCatched;
        public UltEvent OnTrashCatched;

        public UltEvent OnLeftMovement;
        public UltEvent OnRightMovement;
        public UltEvent OnStopMovement;

        [Space]
        public float SpeedMultiplier = 5f;
        public bool CanMove = true;

        private bool _wasTap;
        private Vector2 _speed;


        // ========================================================================================
        /*protected void OnMovement(InputValue value)
        {
            _speed = value.Get<Vector2>();
        }*/

        protected void OnTap(InputValue value)
        {
            _wasTap = value.isPressed;
        }

        protected void OnPointerDelta(InputValue value)
        {
            _speed = value.Get<Vector2>();
        }

        protected void Start()
        {
            Slowmo.Instance.OnSlowmoStart += () => { CanMove = false; };
            Slowmo.Instance.OnSlowmoStop += () => { CanMove = true; };
        }

        protected void Update()
        {
            if (CanMove)
            {
                _speed = _wasTap ? _speed : Vector2.zero;
                transform.position += new Vector3(_speed.x * SpeedMultiplier * Time.unscaledDeltaTime, 0f, 0f);

                if (_speed.x.Approximately(0f))
                    OnStopMovement.Invoke();

                else if (_speed.x < 0f)
                    OnLeftMovement.Invoke();

                else if (_speed.x > 0f)
                    OnRightMovement.Invoke();
            }
            else
                OnStopMovement.Invoke();
        }

        protected void OnCollisionEnter(Collision col)
        {
            var obj = col.gameObject.GetComponent<FallingObject>();
            if (obj && !obj.Processed)
            {
                switch (obj.Key)
                {
                    case FallingObjectKeys.Citizen:
                        LevelManager.Instance.Score++;
                        OnCitizenCatched.Invoke();
                        break;
                    case FallingObjectKeys.Trash:
                        LevelManager.Instance.Health--;
                        OnTrashCatched.Invoke();
                        break;
                    default:
                        break;
                }

                obj.Processed = true;
                Destroy(obj.gameObject);
            }
        }
    }
}