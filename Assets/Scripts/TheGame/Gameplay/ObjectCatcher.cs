using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

// ========================
// Revision 2021.02.15
// ========================

namespace TheGame
{
    public class ObjectCatcher : MonoBehaviour
    {
        // ========================================================================================
        public LayerMask CatchLayers;
        public List<FallingObjectKeys> CatchKeys;

        private FallingObject _currentObject;
        private Vector2 _catchOffset;
        //private Vector3 _catchPosition;
        private Vector3 _targetPosition;


        // ========================================================================================
        protected void OnTap(InputValue value)
        {
            if (!value.isPressed)
                ReleaseObject();
        }

        protected void OnLongTap()
        {
            if (!Slowmo.Instance.IsInSlowmo)
                return;

            var pointerPosition = Pointer.current.position.ReadValue();
            var ray = Camera.main.ScreenPointToRay(pointerPosition);
            if (Physics.Raycast(ray, out var hitInfo, Camera.main.farClipPlane, CatchLayers.value))
            {
                var obj = hitInfo.rigidbody.GetComponent<FallingObject>();
                if (obj && CatchKeys.Contains(obj.Key))
                {
                    CatchObject(obj);

                    var sPos = Camera.main.WorldToScreenPoint(obj.CachedRigidbody.position);
                    _catchOffset = new Vector2(sPos.x - pointerPosition.x, sPos.y - pointerPosition.y);
                }
            }
        }

        protected void Start()
        {
            Slowmo.Instance.OnSlowmoStop += ReleaseObject;
        }

        protected void Update()
        {
            if (_currentObject != null)
            {
                var pointerPosition = Pointer.current.position.ReadValue() - _catchOffset;
                var d = Vector3.Distance(_currentObject.CachedRigidbody.position, Camera.main.transform.position);
                var wPos = Camera.main.ScreenToWorldPoint(new Vector3(pointerPosition.x, pointerPosition.y, d));
                _targetPosition = new Vector3(wPos.x, wPos.y, _currentObject.CachedRigidbody.position.z);
            }
        }

        protected void FixedUpdate()
        {
            if (_currentObject != null)
            {
                var norm = new Vector3(_targetPosition.x - _currentObject.CachedRigidbody.position.x, _targetPosition.y - _currentObject.CachedRigidbody.position.y, 0f).normalized;
                _currentObject.MoveVector = norm;
                
                _currentObject.CachedRigidbody.MovePosition(_targetPosition);
            }
        }

        private void CatchObject(FallingObject obj)
        {
            if (obj)
            {
                ReleaseObject();
             
                _currentObject = obj;
                _currentObject.IsMoving = false;
                _currentObject.OnDestroyed += ReleaseObject;
                _currentObject.OnCatched.Invoke();

                //var pointerPosition = Pointer.current.position.ReadValue();
                //var d = Vector3.Distance(_currentObject.CachedRigidbody.position, Camera.main.transform.position);
                //var wPos = Camera.main.ScreenToWorldPoint(new Vector3(pointerPosition.x, pointerPosition.y, d));
                //_catchOffset = _currentObject.CachedRigidbody.position - wPos;
                //_catchPosition = _currentObject.CachedRigidbody.position;
            }
        }

        private void ReleaseObject()               
        {
            if (_currentObject != null)
            {
                _currentObject.IsMoving = true;
                _currentObject.OnDestroyed -= ReleaseObject;
                _currentObject.OnReleased.Invoke();
                _currentObject = null;
            }
        }
    }
}