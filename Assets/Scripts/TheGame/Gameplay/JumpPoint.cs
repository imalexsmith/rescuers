﻿using UnityEngine;
using MyBox;
#if UNITY_EDITOR
using UnityEditor;
#endif

// ========================
// Revision 2021.02.14
// ========================

namespace TheGame
{
    public class JumpPoint : MonoBehaviour
    {
        // ========================================================================================
        public MinMaxFloat SpeedMultiplierRange = new MinMaxFloat(1f, 1f);
        public MinMaxFloat AccelerationMultiplierRange = new MinMaxFloat(1f, 1f);


        // ========================================================================================
        public FallingObject LaunchObject(FallingObject prefab, float speed, float acceleration)
        {
            var obj = Instantiate(prefab, transform.position, prefab.transform.rotation);
            obj.Speed = speed * SpeedMultiplierRange.RandomInRange();
            obj.Acceleration = acceleration * AccelerationMultiplierRange.RandomInRange();
            obj.IsMoving = true;
            //obj.TargetPosition = new Vector3(transform.position.x, float.MinValue, transform.position.z);

            return obj;
        }

#if UNITY_EDITOR
        protected void OnDrawGizmos()
        {
            Handles.BeginGUI();
            var p = transform.parent;
            var t = "";
            while (p != null)
            {
                t = $"{p.name}\n {t}";
                p = p.parent;
            }
            t += name;

            Handles.Label(transform.position, t);
            Handles.EndGUI();
        }
#endif
    }
}