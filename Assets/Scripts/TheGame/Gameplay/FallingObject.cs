﻿using UnityEngine;
using UltEvents;

// ========================
// Revision 2021.02.15
// ========================

namespace TheGame
{
    [RequireComponent(typeof(Rigidbody))]
    public class FallingObject : MonoBehaviour
    {
        // ========================================================================================
        public UltEvent OnDestroyed;
        public UltEvent OnCatched;
        public UltEvent OnReleased;

        [Space]
        public FallingObjectKeys Key;
        public float Speed;
        public float Acceleration;
        public bool IsMoving;
        public bool Processed;
        public Rigidbody CachedRigidbody;

        public Vector3 MoveVector { get; set; } = Vector3.down;


        // ========================================================================================
        protected void Reset()
        {
            CachedRigidbody = GetComponent<Rigidbody>();
            CachedRigidbody.isKinematic = true;
        }

        protected void Awake()
        {
            if (!CachedRigidbody)
                CachedRigidbody = GetComponent<Rigidbody>();
        }

        protected void FixedUpdate()
        {
            if (IsMoving)
            {
                Speed += Acceleration * Time.deltaTime;
                CachedRigidbody.MovePosition(CachedRigidbody.position + MoveVector * Speed * Time.deltaTime);
            }
        }

        protected void OnDestroy()
        {
            OnDestroyed.Invoke();
        }
    }
}