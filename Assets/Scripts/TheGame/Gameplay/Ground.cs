﻿using UnityEngine;
using UltEvents;

// ========================
// Revision 2021.02.04
// ========================

namespace TheGame
{
    public class Ground : MonoBehaviour
    {
        // ========================================================================================
        public UltEvent OnCitizenCatched;
        public UltEvent OnTrashCatched;

        [Space]
        public GameObject BloodDecalPrefab;

        private ContactPoint _lastContact;


        // ========================================================================================
        public void SpawnBloodDecal()
        {
            Instantiate(BloodDecalPrefab, _lastContact.point, Quaternion.identity);
        }

        protected void OnCollisionEnter(Collision col)
        {
            var obj = col.gameObject.GetComponent<FallingObject>();
            if (obj && !obj.Processed)
            {
                switch (obj.Key)
                {
                    case FallingObjectKeys.Citizen:
                        LevelManager.Instance.Health--;
                        _lastContact = col.GetContact(0);
                        OnCitizenCatched.Invoke();
                        break;
                    case FallingObjectKeys.Trash:
                        OnTrashCatched.Invoke();
                        break;
                    default:
                        break;
                }

                obj.Processed = true;
                Destroy(obj.gameObject);
            }
        }
    }
}
