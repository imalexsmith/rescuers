﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UltEvents;
using NightFramework;

// ========================
// Revision 2021.02.08
// ========================

namespace TheGame
{
    public class LevelManager : Singleton<LevelManager>
    {
        // ========================================================================================
        public UltEvent OnHealthChanged;
        public UltEvent OnHealthEnded;
        public UltEvent OnScoreChanged;
        public UltEvent OnScoreLimitReached;
        public UltEvent OnCurrentBuildingChanged;
        public UltEvent OnLevelComplete;

        [Space]
        public Trampoline Player;
        public int InitialHealth = 3;
        public List<Building> Buildings;

        [Min(0), SerializeField]
        private int _currentBuildingIndex;
        public int CurrentBuildingIndex 
        {
            get => _currentBuildingIndex;
            set
            {
                value = Mathf.Clamp(value, 0, Buildings.Count - 1);
                if (_currentBuildingIndex != value)
                {
                    _currentBuildingIndex = value;
                    _scoreLimit += CurrentBuilding.ScoreLimit;

                    OnCurrentBuildingChanged.Invoke();
                }
            }
        }
        
        private int _score;
        public int Score 
        {
            get => _score;
            set
            {
                if (_score != value)
                {
                    _score = value;
                    OnScoreChanged.Invoke();

                    if (_score >= ScoreLimit)
                    {
                        if (CurrentBuildingIndex + 1 >= Buildings.Count)
                            OnLevelComplete.Invoke();
                        else
                            OnScoreLimitReached.Invoke();
                    }
                }
            }
        }

        private int _scoreLimit;
        public int ScoreLimit => _scoreLimit;

        private int _health;
        public int Health 
        {
            get => _health;
            set
            {
                if (_health != value)
                {
                    _health = value;
                    OnHealthChanged.Invoke();

                    if (_health <= 0)
                        OnHealthEnded.Invoke();
                }
            }
        }

        public Building CurrentBuilding => Buildings[CurrentBuildingIndex];


        // ========================================================================================
        public void NextBuilding()
        {
            CurrentBuildingIndex++;
        }

        public void MoveCamera()
        {
            if (CurrentBuildingIndex + 1 < Buildings.Count)
            {
                Buildings[CurrentBuildingIndex].VirtualCamera.enabled = false;
                Buildings[CurrentBuildingIndex + 1].VirtualCamera.enabled = true;
            }
        }

        public void MovePlayer()
        {
            if (CurrentBuildingIndex + 1 < Buildings.Count)
            {
                Player.transform.position = Buildings[CurrentBuildingIndex + 1].StartPoint.position;
            }
        }

        public void StartCurrentBuilding()
        {
            CurrentBuilding.StartJumping();
        }

        public void StopCurrentBuilding()
        {
            CurrentBuilding.StopJumping();
        }

        public void DisableCurrentBuilding()
        {
            CurrentBuilding.gameObject.SetActive(false);
        }

        public void SpawnCitizenNearCurrentBuilding()
        {
            CurrentBuilding.AddSavedCitizen();
        }

        protected override void Awake()
        {
            base.Awake();

            for (int i = 0; i < Buildings.Count; i++)
            {
                if (i != CurrentBuildingIndex)
                    Buildings[i].VirtualCamera.enabled = false;
            }

            _health = InitialHealth;
            _scoreLimit = CurrentBuilding.ScoreLimit;
        }

        protected void Start()
        {
            CurrentBuilding.StartJumping();
        }

        protected void Update()
        {
            if (Player.transform.position.x < CurrentBuilding.MaxPointLeft.position.x)
                Player.transform.position = new Vector3(CurrentBuilding.MaxPointLeft.position.x, Player.transform.position.y, Player.transform.position.z);

            if (Player.transform.position.x > CurrentBuilding.MaxPointRight.position.x)
                Player.transform.position = new Vector3(CurrentBuilding.MaxPointRight.position.x, Player.transform.position.y, Player.transform.position.z);
        }

#if UNITY_EDITOR                                                                        
        [MyBox.ButtonMethod]
        private void FindBuildingsInScene()
        {
            Buildings = FindObjectsOfType<Building>().ToList();
        }
#endif
    }
}