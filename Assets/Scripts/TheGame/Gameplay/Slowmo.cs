﻿using UnityEngine;
using NightFramework;
using DG.Tweening;
using UltEvents;

// ========================
// Revision 2021.02.05
// ========================

namespace TheGame
{
    public class Slowmo : Singleton<Slowmo>
    {
        // ========================================================================================
        public UltEvent OnSlowmoStart;
        public UltEvent OnSlowmoStop;

        [Space]
        public float MaxDuration = 10f;
        public float FullRefillTime = 40f;
        public float SmoothingTime = 0.55f;
        [Range(0f, 1f)]
        public float Threshold = 0.25f;
        [Range(0f, 1f)]
        public float SlowGameplayFactor = 0.15f;

        public bool IsInSlowmo { get; private set; }
        public float CurrentAmount { get; private set; } = 1f;

        private Tweener _timeScaleTween;


        // ========================================================================================
        public void StartSlowmo()
        {
            if (CurrentAmount < Threshold)
                return;

            IsInSlowmo = true;

            if (_timeScaleTween.IsActive())
                _timeScaleTween.Kill();

            _timeScaleTween = DOTween.To(() => Time.timeScale, x => Time.timeScale = x, SlowGameplayFactor, SmoothingTime);

            OnSlowmoStart.Invoke();
        }

        public void StopSlowmo()
        {
            IsInSlowmo = false;

            if (_timeScaleTween.IsActive())
                _timeScaleTween.Kill();

            _timeScaleTween = DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 1f, SmoothingTime);

            OnSlowmoStop.Invoke();
        }

        protected void Update()
        {
            if (GameplayManager.Instance.IsGameplayPaused || GameplayManager.Instance.UnpauseTimer.Status == SuperTimer.TimerStatus.Active)
                return;

            if (IsInSlowmo)
            {
                CurrentAmount = Mathf.Clamp(CurrentAmount - Time.unscaledDeltaTime / MaxDuration, 0f, 1f);
                if (CurrentAmount == 0f)
                    StopSlowmo();
            }
            else
            {
                CurrentAmount = Mathf.Clamp(CurrentAmount + Time.unscaledDeltaTime / FullRefillTime, 0f, 1f);
            }
        }
    }
}