﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Cinemachine;
using MyBox;
using NightFramework;

// ========================
// Revision 2021.02.08
// ========================

namespace TheGame
{
    [Serializable]
    public class SavedCitizenSpawnPoint
    {
        public Transform Point;
        [NonSerialized]
        public bool Occupied;
    }


    public class Building : MonoBehaviour
    {
        // ========================================================================================
        public MinMaxFloat JumpDelayRange;
        public MinMaxFloat InitialSpeedRange;
        public MinMaxFloat AccelerationRange;
        public int ScoreLimit = 5;
        
        [Space]
        public RandomisedSet<FallingObject> FallingObjectPrefabs;
        public RandomisedSet<JumpPoint> JumpPoints;

        [Space]
        public GameObject SavedCitizenPrefab;
        public List<SavedCitizenSpawnPoint> SavedCitizenSpawnPoints;
        
        [Space]
        public Transform StartPoint;
        public Transform MaxPointLeft;
        public Transform MaxPointRight;
        public CinemachineVirtualCameraBase VirtualCamera;

        private SuperTimer _jumpTimer = new SuperTimer();
        private List<FallingObject> _fallingObjects = new List<FallingObject>();


        // ========================================================================================
        public void StartJumping()
        {
            _jumpTimer.Duration = JumpDelayRange.RandomInRange();
            _jumpTimer.Start();
        }

        public void StopJumping()
        {
            _jumpTimer.Stop();
            for (int i = _fallingObjects.Count - 1; i >= 0; i--)
                _fallingObjects[i].DestroyGameObject();
        }

        public void AddSavedCitizen()
        {
            SavedCitizenSpawnPoint point;
            do {
                point = SavedCitizenSpawnPoints.GetRandom();
            }
            while (point.Occupied);

            Instantiate(SavedCitizenPrefab, point.Point.position, point.Point.rotation, point.Point);
            point.Occupied = true;
        }

        protected void Awake()
        {
            _jumpTimer.NeedOnComplete = true;
            _jumpTimer.OnComplete += Jump;
        }

        protected void Update()
        {
            _jumpTimer.Update();
        }

#if UNITY_EDITOR
        protected void OnValidate()
        {
            FindJumpPoints();
        }
#endif

        private void Jump()
        {
            var prefab = FallingObjectPrefabs.SelectRandomValues()[0];
            var jumpPoint = JumpPoints.SelectRandomValues()[0];
            var fallingObject = jumpPoint.LaunchObject(prefab, InitialSpeedRange.RandomInRange(), AccelerationRange.RandomInRange());

            fallingObject.OnDestroyed += () => { _fallingObjects.Remove(fallingObject); };
            _fallingObjects.Add(fallingObject);

            StartJumping();
        }

#if UNITY_EDITOR
        [MyBox.ButtonMethod]
        private void FindJumpPoints()
        {
            var newPoints = GetComponentsInChildren<JumpPoint>();
            foreach (var point in newPoints)
            {
                if (JumpPoints.Values.Any(x => x.Value == point))
                    continue;

                JumpPoints.Values.Add(new RandomisedSetEntry<JumpPoint> { Value = point });
            }
        }
#endif
    }
}